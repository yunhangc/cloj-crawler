# README #

### What is this repository for? ###

A very simpe single thread crawler to find the path from any wikipedia page to https://en.wikipedia.org/wiki/Philosophy


### How do I get set up? ###

To Set up
> 1. clone the repository
> 2. install leningen : https://leiningen.org/
> 3. go to "crawler" folder, run: lein uberjar
> 4. make sure the java module depend on the uberjar generated.
> 5. in InvokeJava class, change the START_PAGE to the relative page under wiki. Ex. /wiki/Kabbage
> 6. the default maximum crawler depth is 5. To change that, change maxDepth in core.clj
> 7. run the java main method

Results: 

> StartPage : /wiki/Kabbage

> Path: https://en.wikipedia.org/wiki/Kabbage  https://en.wikipedia.org/wiki/Wikipedia:About  https://en.wikipedia.org/wiki/History_of_Wikipedia  https://en.wikipedia.org//wiki/Philosophy