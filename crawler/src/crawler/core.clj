(ns crawler.core
  "crawler routines"
  (:require [org.httpkit.client :as http-kit])
  (:require [crawler.parser :as parser]))

(def crawled-urls (atom {}))

(def reverse-trace (atom ()))

(def prefix "https://en.wikipedia.org/")

(def crawl-queue (atom {}))

(def crawling-queue (atom {}))

(def maxDepth 5)
(def depth (atom 0))

(def philosophy "/wiki/Philosophy")

(defn register-crawled-url
  "Adds a url that has already been crawled to the crawled-urls atom,
  so it will not be crawled again."
  ;; TO DO: keep an eye on the size of the set; could also happen via a watch on the atom
  [url parent]
  (swap! crawled-urls assoc (keyword url) parent)
)


(defn in?
  "true if coll contains elm"
  [coll elm]
  (some #(= elm %) coll))

(defn print-trace [url]
  "add reverse path to reverse-trace atom"
  (if (not ( = url "end"))
    (
      (println url)
      (swap! reverse-trace conj url)
      (print-trace ((keyword url) @crawled-urls))
      )))


(defn crawl
  "Launches the GET of a url in a new future that is then stored in the running-requests atom.
  The future is responsible to remove itself from the atom once the GET is done, so that the
  watch on the atom can launch the next GET (take-url). When the future is realized, it will
  trigger the parsing of the retreived data."
  [url-string parent]
  (
    (register-crawled-url (str prefix url-string) parent)
    (let [crawl-data (parser/do-parse (:body @(http-kit/get (str prefix url-string))) @depth)]
      (println (str prefix url-string) "crawled")
      ;(add-links-to-crawl-queue (:links crawl-data) url-string)
      (let [urls (doall (:links crawl-data))]
        ;(add-links-to-crawl-queue urls url-string)
        (doseq [url urls]
          ;not crawled before
          (if (= ((keyword (str prefix url)) @crawled-urls) nil)
            ;not self-linking
            (if (not (= url (str "/" url-string)))
              (swap! crawl-queue assoc (keyword url) (str prefix url-string))
              )))
        )
      (if (in? (:links crawl-data) philosophy)
        ((register-crawled-url (str prefix philosophy) (str prefix url-string))
          (println "Printing Trace: ")
          (print-trace (str prefix philosophy))
          (throw (Exception. "Philosophy Trace Printed")))))

    )
  )

(defn start
  "start crawling. pass in the start url and maximum crawling depth"
  [url]
  (
    (reset! crawling-queue {(keyword url) "end"})
    (try
      (while (< @depth maxDepth)
        (let [urls (keys @crawling-queue)]
          (doseq [curr-url urls]
            (crawl (name curr-url) ((keyword curr-url) @crawling-queue))
            (swap! crawling-queue dissoc (keyword curr-url))
            ))
        (reset! crawling-queue @crawl-queue)
        (reset! crawl-queue {})
        (swap! depth inc)
        )
      (catch Exception e
        (do (println "\nCrawler shut down. Reason: " (.getMessage e))))
      )
    )
  )

(defn trace [] (java.util.ArrayList. @reverse-trace))