import clojure.java.api.Clojure;
import clojure.lang.IFn;

import java.util.Collections;
import java.util.List;

/**
 * Created by yunhangchen on 7/19/17.
 */
public class InvokeJava {

    public static final String START_PAGE = "/wiki/Kabbage";

    public void invokeMethod() {
        IFn require = Clojure.var("clojure.core", "require");
        require.invoke(Clojure.read("crawler.core"));
        IFn start = Clojure.var("crawler.core", "start");
        start.invoke(START_PAGE);
        IFn trace = Clojure.var("crawler.core", "trace");
        List<String> traceList = (List<String>) (trace.invoke());
        traceList.forEach(str -> System.out.println(str));
    }

    public static void main(String[] args) {
        InvokeJava crawler = new InvokeJava();
        crawler.invokeMethod();
    }

}
